﻿Public Class MDIPrincipal

    Private Sub AgregarToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AgregarToolStripMenuItem.Click
        Dim frm As New Frm_Parametro
        frm.ShowDialog()
    End Sub

    Private Sub TiToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Sub Establecer_Permisos()
        Try

            If sesion_idUsuario = 0 Then
                'ARCHIVO
                IniciarSesiónToolStripMenuItem.Enabled = True
                CerrarSesiónToolStripMenuItem.Enabled = False

                'ENCOMIENDA
                NuevaEncomiendaToolStripMenuItem.Enabled = False
                ConToolStripMenuItem.Enabled = False
                HistorialEncomiendaToolStripMenuItem.Enabled = False
                ListaEncomiendasToolStripMenuItem.Enabled = False

                'CLIENTE
                NuevoClienteToolStripMenuItem.Enabled = False
                ListaDeClientesToolStripMenuItem.Enabled = False

                'PERSONAL
                AgregarToolStripMenuItem1.Enabled = False
                ListaDeUsuariosToolStripMenuItem.Enabled = False
                AgregarToolStripMenuItem3.Enabled = False
                ListaDeChoferesToolStripMenuItem.Enabled = False

                'EDICION
                AgregarToolStripMenuItem.Enabled = False
                AgregarToolStripMenuItem2.Enabled = False
                ListaPuntosToolStripMenuItem.Enabled = False
                AgregarToolStripMenuItem6.Enabled = False
                ListaRutasToolStripMenuItem.Enabled = False
                NuevaFlotaToolStripMenuItem1.Enabled = False
                ListaFlotasToolStripMenuItem.Enabled = False

                'CONFIGURACION
                AgregarToolStripMenuItem4.Enabled = False
                AgregarToolStripMenuItem5.Enabled = False
            Else

                Dim _user As New LCN.Usuario
                Dim tabla As New DataTable

                tabla = _user.ObtenerPermisos(sesion_TipoUser)
                'ARCHIVO
                CerrarSesiónToolStripMenuItem.Enabled = True

                For Each Fila As DataRow In tabla.Rows
                    Select Case Fila.Item("Nombre")
                        Case "INICIAR SESION"
                            IniciarSesiónToolStripMenuItem.Enabled = Fila.Item("Acceso")

                            'ENCOMIENDA
                        Case "NUEVA ENCOMIENDA"
                            NuevaEncomiendaToolStripMenuItem.Enabled = Fila.Item("Acceso")
                        Case "CONFIRMA ENCOMIENDA"
                            ConToolStripMenuItem.Enabled = Fila.Item("Acceso")
                        Case "HISTORIAL ENCOMIENDA"
                            HistorialEncomiendaToolStripMenuItem.Enabled = Fila.Item("Acceso")
                        Case "LISTA ENCOMIENDAS"
                            ListaEncomiendasToolStripMenuItem.Enabled = Fila.Item("Acceso")

                            'CLIENTE
                        Case "NUEVO CLIENTE"
                            NuevoClienteToolStripMenuItem.Enabled = Fila.Item("Acceso")
                        Case "LISTA DE CLIENTES"
                            ListaDeClientesToolStripMenuItem.Enabled = Fila.Item("Acceso")

                            'PERSONAL
                        Case "USUARIO AGREGAR"
                            AgregarToolStripMenuItem1.Enabled = Fila.Item("Acceso")
                        Case "USUARIO LISTA DE USUARIOS"
                            ListaDeUsuariosToolStripMenuItem.Enabled = Fila.Item("Acceso")
                        Case "CHOFER AGREGAR"
                            AgregarToolStripMenuItem3.Enabled = Fila.Item("Acceso")
                        Case "CHOFER LISTA DE CHOFERES"
                            ListaDeChoferesToolStripMenuItem.Enabled = Fila.Item("Acceso")

                            'EDICION
                        Case "PARAMETRO AGREGAR"
                            AgregarToolStripMenuItem.Enabled = Fila.Item("Acceso")
                        Case "PUNTOS AGREGAR"
                            AgregarToolStripMenuItem2.Enabled = Fila.Item("Acceso")
                        Case "PUNTOS LISTA PUNTOS"
                            ListaPuntosToolStripMenuItem.Enabled = Fila.Item("Acceso")
                        Case "HOJA RUTA AGREGAR"
                            AgregarToolStripMenuItem6.Enabled = Fila.Item("Acceso")
                        Case "HOJA RUTA LISTA RUTAS"
                            ListaRutasToolStripMenuItem.Enabled = Fila.Item("Acceso")
                        Case "FLOTA NUEVA FLOTA"
                            NuevaFlotaToolStripMenuItem1.Enabled = Fila.Item("Acceso")
                        Case "FLOTA LISTA FLOTAS"
                            ListaFlotasToolStripMenuItem.Enabled = Fila.Item("Acceso")

                            'CONFIGURACION
                        Case "PERMISOS AGREGAR"
                            AgregarToolStripMenuItem4.Enabled = Fila.Item("Acceso")
                        Case "TIPO DE USUARIO AGREGAR"
                            AgregarToolStripMenuItem5.Enabled = Fila.Item("Acceso")

                    End Select
                Next
             
            End If
          

        Catch ex As Exception
            MsgBox("Error al intentar establecer los permisos", MsgBoxStyle.Critical)
        End Try
    End Sub
    Private Sub NuevaEncomiendaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NuevaEncomiendaToolStripMenuItem.Click
        Dim frm As New Frm_Encomienda
        frm.ShowDialog()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub NuevoClienteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles NuevoClienteToolStripMenuItem.Click
        Dim frm As New Frm_Cliente
        frm.ShowDialog()
    End Sub

    Private Sub IniciarSesiónToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles IniciarSesiónToolStripMenuItem.Click
        Dim frm As New Frm_Login
        frm.ShowDialog()
    End Sub

    Private Sub AgregarToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles AgregarToolStripMenuItem1.Click
        Dim frm As New Frm_Usuario
        frm.ShowDialog()
    End Sub

    Private Sub AgregarToolStripMenuItem3_Click(sender As System.Object, e As System.EventArgs) Handles AgregarToolStripMenuItem3.Click
        Dim frm As New Frm_Chofer
        frm.ShowDialog()
    End Sub

    Private Sub NuevaFlotaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        Dim frm As New Frm_Flota
        frm.ShowDialog()
    End Sub

    Private Sub AgregarToolStripMenuItem2_Click(sender As System.Object, e As System.EventArgs) Handles AgregarToolStripMenuItem2.Click
        Dim frm As New Frm_Punto
        frm.ShowDialog()
    End Sub

    Private Sub AgregarToolStripMenuItem6_Click(sender As System.Object, e As System.EventArgs) Handles AgregarToolStripMenuItem6.Click
        Dim frm As New Frm_HojaRuta
        frm.ShowDialog()
    End Sub

    Private Sub AgregarToolStripMenuItem5_Click(sender As System.Object, e As System.EventArgs) Handles AgregarToolStripMenuItem5.Click
        Dim frm As New Frm_TipoUsuario
        frm.ShowDialog()
    End Sub

    Private Sub AgregarToolStripMenuItem4_Click(sender As System.Object, e As System.EventArgs) Handles AgregarToolStripMenuItem4.Click
        Dim frm As New Frm_Permisos
        frm.ShowDialog()
    End Sub

    Private Sub ListaDeClientesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListaDeClientesToolStripMenuItem.Click
        Dim frm As New Frm_ListaClientes
        frm.ShowDialog()
    End Sub

    Private Sub MDIPrincipal_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Timer1.Start()
        lbl_hora.Text = Date.Now
        tss_fecha.Text = Date.Now()
        Me.Establecer_Permisos()
        Dim frm As New Frm_Login
        frm.ShowDialog()

    End Sub

    Private Sub ListaDeFlotasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        Dim frm As New Frm_ListaFlotas
        frm.ShowDialog()
    End Sub

    Private Sub ListaDeChoferesToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListaDeChoferesToolStripMenuItem.Click
        Dim frm As New Frm_ListaChoferes
        frm.ShowDialog()
    End Sub

    Private Sub ListaDeUsuariosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListaDeUsuariosToolStripMenuItem.Click
        Dim frm As New Frm_ListaUsuarios
        frm.ShowDialog()
    End Sub

    Private Sub ConToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ConToolStripMenuItem.Click
        Dim frm As New Frm_Confirmar
        frm.ShowDialog()
    End Sub

    Private Sub NuevaFlotaToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles NuevaFlotaToolStripMenuItem1.Click
        Dim frm As New Frm_Flota
        frm.ShowDialog()
    End Sub

    Private Sub ListaFlotasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListaFlotasToolStripMenuItem.Click
        Dim frm As New Frm_ListaFlotas
        frm.ShowDialog()
    End Sub

    'Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
    '    tss_fecha.Text = Date.Now()
    'End Sub

    Private Sub HistorialEncomiendaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HistorialEncomiendaToolStripMenuItem.Click
        Dim frm As New Frm_HistorialEncomienda
        frm.ShowDialog()
    End Sub


    Private Sub ListaPuntosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaPuntosToolStripMenuItem.Click
        Dim frm As New Frm_listaPuntos
        frm.ShowDialog()
    End Sub

    Private Sub ListaEncomiendasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaEncomiendasToolStripMenuItem.Click
        Dim frm As New Frm_ListaEncomiendas
        frm.ShowDialog()
    End Sub

    Private Sub ListaRutasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaRutasToolStripMenuItem.Click
        Dim frm As New Frm_List_HojaRuta
        frm.ShowDialog()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Start()
        lbl_hora.Text = Date.Now

    End Sub
End Class