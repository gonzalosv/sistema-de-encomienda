﻿Public Class Frm_HistorialEncomienda
    '*Instancia de la Clase Parametro
    Dim he As New LCN.HistorialEncomienda

    Private _idHojaRuta As Integer
    Public OtroIDAux As Integer
    '*Variable que verifica si va a guardar o Actualizar
    Dim nuevo As Boolean = True

    Private Sub Btn_Lista_Flotas_Click_1(sender As Object, e As EventArgs)
        Dim frm As New Frm_ListaRutas
        frm.ShowDialog()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim frm As New Frm_lista_ruta_Origen
            'frm.lbl_titulo.Text = "Lista De Rutas"
            frm.aux = 1
            frm.ShowDialog()
            Me.txt_idRuta.Text = frm.nom
            Me._idHojaRuta = frm.id_hoja_ruta
            OtroIDAux = frm.ID_Origen
            cmb_estado_SelectedIndexChanged(sender, e)
            Cargar_ComboEstado()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Sub Cargar_ComboEstado()

        Dim IDAux As New LCD.CAD

        Me.cmb_estado.Items.Clear()

        If IDAux.ID_PuntoGlobal = OtroIDAux Then
            Me.cmb_estado.Items.Add("Cargado")
            Me.cmb_estado.Items.Add("Transito")
        Else
            Me.cmb_estado.Items.Add("Destino")
            Me.cmb_estado.Items.Add("Entregado")
        End If

    End Sub

    Sub CargarDataGrid(ByVal idHojaRuta As Integer, ByVal estado As Char)
        Dim TablaEncomienda As New DataTable
        Dim encomienda As New LCN.Encomienda

        TablaEncomienda = encomienda.GetForRouteSheet(idHojaRuta, estado)

        If Not IsNothing(TablaEncomienda) Then
            Me.dgv_historialencomienda.DataSource = TablaEncomienda

            For Each Fila As DataGridViewRow In Me.dgv_historialencomienda.Rows
                If Fila.Cells("Estado").Value = 1 Then
                    Fila.Cells("Estados").Value = "Recibido"
                ElseIf Fila.Cells("Estado").Value = 2 Then
                    Fila.Cells("Estados").Value = "Transito"
                ElseIf Fila.Cells("Estado").Value = 3 Then
                    Fila.Cells("Estados").Value = "En camino"
                ElseIf Fila.Cells("Estado").Value = 4 Then
                    Fila.Cells("Estados").Value = "Entregado"
                End If
            Next

        Else
            For i As Integer = 0 To Me.dgv_historialencomienda.Rows.Count - 1
                Me.dgv_historialencomienda.Rows.RemoveAt(0)
            Next
        End If
        Me.dgv_historialencomienda.ClearSelection()
    End Sub


    Private Sub Frm_HistorialEncomienda_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.cmb_estado.SelectedIndex = 0
    End Sub

    Private Sub txt_Flota_TextChanged(sender As Object, e As EventArgs) Handles txt_idRuta.TextChanged
    End Sub

    Sub GuardarHistorial()
        For Each Fila As DataGridViewRow In Me.dgv_historialencomienda.Rows
            Dim encomienda As New LCN.Encomienda
            Try
                If Fila.Cells("Confirmado").Value Then
                    If Fila.Cells("Estado").Value = 4 Then
                        Dim frmReport As New frmVisualizadorRPT
                        Me.Cursor = Cursors.WaitCursor
                        Dim DT As New DataTable
                        Dim nro_guia As String
                        Try
                            nro_guia = Fila.Cells("ID_Encomienda").Value
                            DT = encomienda.ReciboEntrega(nro_guia)
                            If IsNothing(DT) Then
                                Me.Cursor = Cursors.Default
                                MsgBox("No Existen Datos a Mostrar", MsgBoxStyle.Information, "ENCOMIENDAS")
                            Else
                                Dim RepTemp As New rtpReciboEncomiendaEntrega
                                RepTemp.SetDataSource(DT)
                                frmReport.crviewer.ReportSource = RepTemp
                                frmReport.ShowDialog()
                                RepTemp.Dispose()
                            End If
                            Me.Cursor = Cursors.Default
                        Catch ex As Exception
                            Me.Cursor = Cursors.Default
                            MsgBox("ERROR DESDE :: " & ex.Source & vbNewLine & " MENSAJE :: " & ex.Message, MsgBoxStyle.Critical, "ENCOMIENDAS")
                        End Try
                    End If
                    encomienda.ID_Encomienda = Fila.Cells("ID_Encomienda").Value
                    Dim estado = Fila.Cells("Estado").Value + 1
                    encomienda.Estado = estado.ToString()
                    encomienda.UpdateState()
                    MsgBox(Fila.Cells("Descripcion").Value + "... Cambió a estado: " + cmb_estado.SelectedItem)

                End If

            Catch ex As Exception
                MsgBox("No puede cambiar este estado")
            End Try

        Next
    End Sub

    Function ObtenerEstado() As Integer

        Dim Estado As Integer
        Select Case Me.cmb_estado.Text
            Case "Cargado"
                Estado = 1
            Case "Transito"
                Estado = 2
            Case "Destino"
                Estado = 3
            Case "Entregado"
                Estado = 4
        End Select
        Return Estado
    End Function

    Private Sub btn_guardar_Click(sender As Object, e As EventArgs) Handles btn_guardar.Click
        GuardarHistorial()
        CargarDataGrid(Me._idHojaRuta, Me.ObtenerEstado().ToString)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        If dgv_historialencomienda.CurrentRow.Cells(3).Value.Equals(True) Then
            MsgBox("esta activo")
        Else
            MsgBox("no esta activo")
        End If
    End Sub

    Private Sub cmb_estado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmb_estado.SelectedIndexChanged
        Try
            CargarDataGrid(Me._idHojaRuta, Me.ObtenerEstado().ToString)
        Catch ex As Exception
            MsgBox("Error: " + ex.Message)
        End Try
        
    End Sub

    Private Sub GenerarReciboEntrega()
        
    End Sub
    Private Sub btnReporte_Click(sender As Object, e As EventArgs)

    End Sub
End Class