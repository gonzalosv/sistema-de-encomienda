﻿Imports LCN

Public Class Frm_HojaRuta

    Private Sub Frm_HojaRuta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarOrigen()
    End Sub


    Dim hr As New HojaRuta
    Dim histR As New HistorialRuta
    Public nuevo As Boolean = True
    Public Flota As String = ""
    Public Chofer As String = ""

    Sub CargarOrigen()
        Dim TablaPuntos As New DataTable
        TablaPuntos = hr.BusquedaOrigen()
        If Not IsNothing(TablaPuntos) Then
            Me.cbOrigen.DataSource = TablaPuntos
            Me.cbOrigen.DisplayMember = "Descripcion"
            Me.cbOrigen.ValueMember = "ID_Punto"
            CargarDestino(cbOrigen.SelectedValue)
        Else
        End If
    End Sub

    Sub CargarDestino(ByVal des As Integer)
        Dim TablaPuntos As New DataTable
        TablaPuntos = hr.BusquedaDestino(des)
        If Not IsNothing(TablaPuntos) Then
            Me.cbDestino.DataSource = TablaPuntos
            Me.cbDestino.DisplayMember = "Descripcion"
            Me.cbDestino.ValueMember = "ID_Punto"
        Else
        End If
    End Sub

    Private Sub PierdeFoco(ByVal sender As TextBox, ByVal e As System.EventArgs) Handles txt_Flota.LostFocus, txt_Chofer.LostFocus
        sender.BackColor = Color.White
        sender.SelectAll()
    End Sub

    Private Sub AgarraFoco(ByVal sender As TextBox, ByVal e As System.EventArgs) Handles txt_Flota.GotFocus, txt_Chofer.GotFocus
        sender.BackColor = Color.LightCyan
        sender.SelectAll()
    End Sub

    Private Sub btn_guardar_Click(sender As Object, e As EventArgs) Handles btn_guardar.Click
        If nuevo Then

            If txt_Flota.Text.Length > 0 And txt_Chofer.Text.Length > 0 And cbOrigen.Text.Length > 0 And cbDestino.Text.Length > 0 Then

                hr.IdFlota = Flota
                hr.NroDoc = Chofer
                hr.IdOrigen = cbOrigen.SelectedValue
                hr.IdDestino = cbDestino.SelectedValue
                hr.FechaSalida = dtFechaSalida.Value
                hr.FechaLlegada = DtFechaLlegada.Value
                histR.IdUsuario = sesion_idUsuario

                If hr.Guardar() Then
                    histR.Guardar()
                    MsgBox("Guardado Correctamente", MsgBoxStyle.Information, "CHOFERES")
                    txt_Flota.Clear()
                    txt_Chofer.Clear()
                Else
                    MsgBox("Ocurrio Algun Problema", MsgBoxStyle.Critical)
                End If
            Else
                MsgBox("Faltan Campos Obligatorios", MsgBoxStyle.Critical)
                If txt_Flota.Text.Length = 0 Then
                    txt_Flota.Focus()
                ElseIf txt_Chofer.Text.Length = 0 Then
                    txt_Chofer.Focus()
                End If
            End If
        Else
            If txt_Flota.Text.Length > 0 And txt_Chofer.Text.Length > 0 And cbOrigen.Text.Length > 0 And cbDestino.Text.Length > 0 Then

                'debo modificar el identificador de la tabla de flotas
                hr.IdHojaRuta = 1
                hr.IdFlota = Flota
                hr.NroDoc = txt_Chofer.Text
                MsgBox(cbDestino.SelectedValue)
                hr.IdOrigen = cbOrigen.SelectedValue
                hr.IdDestino = cbDestino.SelectedValue
                hr.FechaSalida = dtFechaSalida.Value
                hr.FechaLlegada = DtFechaLlegada.Value

                If hr.Editar() Then
                    MsgBox("Modificado Correctamente", MsgBoxStyle.Information, "CHOFERES")
                    Me.Close()
                Else
                    MsgBox("Ocurrio Algun Problema", MsgBoxStyle.Critical)
                End If
            Else
                MsgBox("Faltan Campos Obligatorios", MsgBoxStyle.Critical)
                If txt_Flota.Text.Length = 0 Then
                    txt_Flota.Focus()
                ElseIf txt_Chofer.Text.Length = 0 Then
                    txt_Chofer.Focus()
                End If
            End If
        End If
    End Sub

    Public Ci As New Integer
    Private Sub Btn_Lista_Flotas_Click_1(sender As Object, e As EventArgs) Handles Btn_Lista_Flotas.Click
        Try
            Dim frm As New Frm_SelectFlota
            frm.orig = cbOrigen.SelectedValue
            frm.ShowDialog()
            Me.txt_Flota.Text = frm.Placa
            Me.Flota = frm.ID_Flota
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub BtnChoferes_Click(sender As Object, e As EventArgs) Handles BtnChoferes.Click
        Try
            Dim frm As New Frm_SelectChofer
            frm.orin = cbOrigen.SelectedValue
            frm.ShowDialog()
            Me.txt_Chofer.Text = frm.Chofer
            Me.Chofer = frm.CI_Chofer
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub cbOrigen_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cbOrigen.SelectionChangeCommitted
        CargarDestino(cbOrigen.SelectedValue)
        txt_Chofer.Text = ""
        txt_Flota.Text = ""
    End Sub
End Class