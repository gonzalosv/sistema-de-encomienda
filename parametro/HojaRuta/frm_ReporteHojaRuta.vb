﻿Imports LCN

Public Class frm_ReporteHojaRuta

    Private Sub frm_ReporteHojaRuta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarOrigen()
        If (id_punto <> 1) Then
            cbPunto.SelectedValue = id_punto
            cbPunto.Enabled = False
        End If
    End Sub

    Dim hr As New HojaRuta
    Sub CargarOrigen()
        Dim TablaPuntos As New DataTable
        TablaPuntos = hr.BusquedaOrigen()
        If Not IsNothing(TablaPuntos) Then
            Me.cbPunto.DataSource = TablaPuntos
            Me.cbPunto.DisplayMember = "Descripcion"
            Me.cbPunto.ValueMember = "ID_Punto"
        Else
        End If
    End Sub

    Private Sub btn_reporte_Click(sender As Object, e As EventArgs) Handles btn_reporte.Click
        Try

            Dim frmReport As New frmVisualizadorRPT
            Me.Cursor = Cursors.WaitCursor

            Dim _hr As New LCN.HojaRuta
            Dim DT As New DataTable

            Dim x As DateTime = dtinicio.Value.Date + " 00:00:00"
            Dim y As DateTime = dtfin.Value.Date + " 00:00:00"

            DT = _hr.Reporte(cbPunto.SelectedValue, x, y)

            If IsNothing(DT) Then
                Me.Cursor = Cursors.Default
                MsgBox("No Existen Datos a Mostrar", MsgBoxStyle.Information, "ENCOMIENDAS")
            Else
                Dim RepTemp As New rptHojasRuta
                RepTemp.SetDataSource(DT)
                frmReport.crviewer.ReportSource = RepTemp
                frmReport.ShowDialog()
                RepTemp.Dispose()

            End If
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("ERROR DESDE :: " & ex.Source & vbNewLine & " MENSAJE :: " & ex.Message, MsgBoxStyle.Critical, "ENCOMIENDAS")
        End Try
    End Sub
End Class