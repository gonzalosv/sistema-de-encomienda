﻿Public Class Frm_SelectFlota

    Public orig As New Integer
    Public Placa As String = ""
    Public ID_Flota As String = ""
    Private Sub Frm_SelectFlota_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarGrid(orig)
    End Sub

    Dim fr As New Frm_HojaRuta
    Sub CargarGrid(ByVal origen As Integer)
        Dim TablaFlota As New DataTable
        Dim Flo As New LCN.Flota

        TablaFlota = Flo.ObtenerPorOrigen(origen)

        If Not IsNothing(TablaFlota) Then
            Me.dgFlotas.DataSource = TablaFlota
        Else
            For i As Integer = 0 To Me.dgFlotas.Rows.Count - 1
                Me.dgFlotas.Rows.RemoveAt(0)
            Next
        End If
        Me.dgFlotas.ClearSelection()
    End Sub

    Private Sub dgFlotas_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgFlotas.CellDoubleClick
        If Me.dgFlotas.CurrentRow.Selected = False Then
            Me.Placa = Me.dgFlotas.CurrentRow.Cells("Pla").Value
            Me.ID_Flota = Me.dgFlotas.CurrentRow.Cells("ID").Value
            Me.Close()
        End If
    End Sub
End Class