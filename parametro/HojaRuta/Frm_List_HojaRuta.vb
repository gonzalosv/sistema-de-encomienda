﻿Imports LCN
Public Class Frm_List_HojaRuta
    Dim hr As New HojaRuta
    Dim frm As New Frm_HojaRuta

    Public HojaRuta As String = ""
    Public origenDestino As String = ""
    Public IdHojaRuta As Integer
    'Public id_HojaRuta As Integer
    Private Sub Frm_List_HojaRuta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarGrid(1)
    End Sub
    Sub CargarGrid(ByVal Activo As Boolean)
        Dim Tabla As New DataTable
        Tabla = hr.Busqueda(Activo)

        If Not IsNothing(Tabla) Then
            Me.dgv_HojaRuta.DataSource = Tabla
        Else
            For i As Integer = 0 To Me.dgv_HojaRuta.Rows.Count - 1
                Me.dgv_HojaRuta.Rows.RemoveAt(0)
            Next
        End If
    End Sub

    Private Function Estado() As Boolean
        If cb_Estado.Text = "ACTIVOS" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub cb_Estado_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_Estado.TextChanged
        CargarGrid(Estado)
    End Sub

    Private Sub btn_cancelar_Click(sender As Object, e As EventArgs) Handles btn_cancelar.Click
        Me.Close()
    End Sub

    Private Sub dgv_HojaRuta_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_HojaRuta.CellDoubleClick
        If Me.dgv_HojaRuta.CurrentRow.Selected = False Then
            Me.origenDestino = Me.dgv_HojaRuta.CurrentRow.Cells("Origen").Value + " - " + Me.dgv_HojaRuta.CurrentRow.Cells("Destino").Value
            Me.IdHojaRuta = Me.dgv_HojaRuta.CurrentRow.Cells("ID_HojaRuta").Value
            Me.Close()
        End If
    End Sub

    Private Sub dgv_HojaRuta_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_HojaRuta.CellContentClick

    End Sub

    Private Sub dgv_HojaRuta_MouseDown(sender As Object, e As MouseEventArgs) Handles dgv_HojaRuta.MouseDown
        If e.Button = MouseButtons.Right Then
            With Me.dgv_HojaRuta
                Dim Hitest As DataGridView.HitTestInfo = .HitTest(e.X, e.Y)
                If Hitest.Type = DataGridViewHitTestType.Cell Then
                    .CurrentCell = .Rows(Hitest.RowIndex).Cells(Hitest.ColumnIndex)
                    .ContextMenuStrip = Me.ContextMenuStrip1
                End If
            End With
        Else
            Me.dgv_HojaRuta.ContextMenuStrip = Nothing
        End If
    End Sub

    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        Dim hoja As New LCN.HojaRuta
        Dim con As New LCD.CAD
        Me.Cursor = Cursors.WaitCursor
        If hoja.migracionHojaRuta(con.db_host) Then
            MsgBox("Hojas de rutas enviadas")
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub BtnGuardar_Click(sender As Object, e As EventArgs) Handles BtnGuardar.Click
        Dim frm As New Frm_HojaRuta
        frm.ShowDialog()
    End Sub

    Private Sub BtnReporte_Click(sender As Object, e As EventArgs) Handles BtnReporte.Click
        Try
            If Me.dgv_HojaRuta.RowCount - 1 < 0 Then
                MsgBox("No tiene ningún reporte disponible.", MsgBoxStyle.Critical, "ENCOMIENDAS")
            Else
                Dim frmReport As New frmVisualizadorRPT
                Me.Cursor = Cursors.WaitCursor

                Dim _HR As New LCN.HojaRuta
                Dim DT As New DataTable

                DT = _HR.Busqueda(Me.cb_Estado.SelectedIndex)

                If IsNothing(DT) Then
                    Me.Cursor = Cursors.Default
                    MsgBox("No Existen Datos a Mostrar", MsgBoxStyle.Information, "ENCOMIENDAS")
                Else
                    Dim RepTemp As New rptHojaRuta
                    RepTemp.SetDataSource(DT)
                    frmReport.crviewer.ReportSource = RepTemp
                    frmReport.ShowDialog()
                    RepTemp.Dispose()

                End If
                Me.Cursor = Cursors.Default

            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("ERROR DESDE :: " & ex.Source & vbNewLine & " MENSAJE :: " & ex.Message, MsgBoxStyle.Critical, "ENCOMIENDAS")
        End Try
    End Sub
End Class