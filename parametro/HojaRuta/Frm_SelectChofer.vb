﻿Public Class Frm_SelectChofer

    Public orin As New Integer
    Public Chofer As String = ""
    Public CI_Chofer As String = ""
    Private Sub Frm_SelectChofer_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarGrid(orin)
    End Sub
    Sub CargarGrid(ByVal origen As Integer)
        Dim TablaFlota As New DataTable
        Dim Cho As New LCN.Chofer

        TablaFlota = Cho.ChoferPorOrigen(origen)

        If Not IsNothing(TablaFlota) Then
            Me.dgv_Choferes.DataSource = TablaFlota
        Else
            For i As Integer = 0 To Me.dgv_Choferes.Rows.Count - 1
                Me.dgv_Choferes.Rows.RemoveAt(0)
            Next
        End If
        Me.dgv_Choferes.ClearSelection()
    End Sub

    Private Sub dgv_Choferes_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_Choferes.CellDoubleClick
        If Me.dgv_Choferes.CurrentRow.Selected = False Then
            Me.Chofer = Me.dgv_Choferes.CurrentRow.Cells("Nombre").Value
            Me.CI_Chofer = Me.dgv_Choferes.CurrentRow.Cells("NroDoc").Value
            Me.Close()
        End If
    End Sub
End Class