﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MDIPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDIPrincipal))
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tss_fecha = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IniciarSesiónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CerrarSesiónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EncomiendaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevaEncomiendaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistorialEncomiendaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaEncomiendasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PersonalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuarioToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeUsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChoferToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeChoferesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguracionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ParametroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PuntosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaPuntosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HojaRutaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarToolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaRutasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FlotaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevaFlotaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaFlotasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguracionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PermisosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoDeUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarToolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lbl_hora = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel2, Me.tss_fecha})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 576)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1077, 22)
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel2.ForeColor = System.Drawing.Color.Gray
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(55, 17)
        Me.ToolStripStatusLabel2.Text = "Estado:"
        '
        'tss_fecha
        '
        Me.tss_fecha.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tss_fecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.999999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tss_fecha.ForeColor = System.Drawing.Color.Gray
        Me.tss_fecha.Name = "tss_fecha"
        Me.tss_fecha.Size = New System.Drawing.Size(46, 17)
        Me.tss_fecha.Text = "Fecha"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Gold
        Me.MenuStrip1.Font = New System.Drawing.Font("Candara", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.EncomiendaToolStripMenuItem, Me.ClienteToolStripMenuItem, Me.PersonalToolStripMenuItem, Me.ConfiguracionToolStripMenuItem, Me.ConfiguracionToolStripMenuItem1})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1077, 26)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IniciarSesiónToolStripMenuItem, Me.CerrarSesiónToolStripMenuItem, Me.ToolStripSeparator1, Me.SalirToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(69, 22)
        Me.ArchivoToolStripMenuItem.Text = "Archivo"
        '
        'IniciarSesiónToolStripMenuItem
        '
        Me.IniciarSesiónToolStripMenuItem.Image = CType(resources.GetObject("IniciarSesiónToolStripMenuItem.Image"), System.Drawing.Image)
        Me.IniciarSesiónToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.IniciarSesiónToolStripMenuItem.Name = "IniciarSesiónToolStripMenuItem"
        Me.IniciarSesiónToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.IniciarSesiónToolStripMenuItem.Text = "Iniciar Sesión"
        '
        'CerrarSesiónToolStripMenuItem
        '
        Me.CerrarSesiónToolStripMenuItem.Image = CType(resources.GetObject("CerrarSesiónToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CerrarSesiónToolStripMenuItem.Name = "CerrarSesiónToolStripMenuItem"
        Me.CerrarSesiónToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.CerrarSesiónToolStripMenuItem.Text = "Cerrar Sesión"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(159, 6)
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Image = CType(resources.GetObject("SalirToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'EncomiendaToolStripMenuItem
        '
        Me.EncomiendaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevaEncomiendaToolStripMenuItem, Me.ConToolStripMenuItem, Me.HistorialEncomiendaToolStripMenuItem, Me.ListaEncomiendasToolStripMenuItem})
        Me.EncomiendaToolStripMenuItem.Name = "EncomiendaToolStripMenuItem"
        Me.EncomiendaToolStripMenuItem.Size = New System.Drawing.Size(98, 22)
        Me.EncomiendaToolStripMenuItem.Text = "Encomienda"
        '
        'NuevaEncomiendaToolStripMenuItem
        '
        Me.NuevaEncomiendaToolStripMenuItem.Image = CType(resources.GetObject("NuevaEncomiendaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NuevaEncomiendaToolStripMenuItem.Name = "NuevaEncomiendaToolStripMenuItem"
        Me.NuevaEncomiendaToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.NuevaEncomiendaToolStripMenuItem.Text = "Nueva Encomienda"
        '
        'ConToolStripMenuItem
        '
        Me.ConToolStripMenuItem.Image = CType(resources.GetObject("ConToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ConToolStripMenuItem.Name = "ConToolStripMenuItem"
        Me.ConToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.ConToolStripMenuItem.Text = "Confirma Encomienda"
        '
        'HistorialEncomiendaToolStripMenuItem
        '
        Me.HistorialEncomiendaToolStripMenuItem.Image = CType(resources.GetObject("HistorialEncomiendaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.HistorialEncomiendaToolStripMenuItem.Name = "HistorialEncomiendaToolStripMenuItem"
        Me.HistorialEncomiendaToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.HistorialEncomiendaToolStripMenuItem.Text = "Historial Encomienda"
        '
        'ListaEncomiendasToolStripMenuItem
        '
        Me.ListaEncomiendasToolStripMenuItem.Name = "ListaEncomiendasToolStripMenuItem"
        Me.ListaEncomiendasToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.ListaEncomiendasToolStripMenuItem.Text = "Lista Encomiendas"
        '
        'ClienteToolStripMenuItem
        '
        Me.ClienteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoClienteToolStripMenuItem, Me.ListaDeClientesToolStripMenuItem})
        Me.ClienteToolStripMenuItem.Name = "ClienteToolStripMenuItem"
        Me.ClienteToolStripMenuItem.Size = New System.Drawing.Size(65, 22)
        Me.ClienteToolStripMenuItem.Text = "Cliente"
        '
        'NuevoClienteToolStripMenuItem
        '
        Me.NuevoClienteToolStripMenuItem.Image = CType(resources.GetObject("NuevoClienteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NuevoClienteToolStripMenuItem.Name = "NuevoClienteToolStripMenuItem"
        Me.NuevoClienteToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.NuevoClienteToolStripMenuItem.Text = "Nuevo Cliente"
        '
        'ListaDeClientesToolStripMenuItem
        '
        Me.ListaDeClientesToolStripMenuItem.Image = CType(resources.GetObject("ListaDeClientesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ListaDeClientesToolStripMenuItem.Name = "ListaDeClientesToolStripMenuItem"
        Me.ListaDeClientesToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.ListaDeClientesToolStripMenuItem.Text = "Lista de Clientes"
        '
        'PersonalToolStripMenuItem
        '
        Me.PersonalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UsuarioToolStripMenuItem1, Me.ChoferToolStripMenuItem1})
        Me.PersonalToolStripMenuItem.Name = "PersonalToolStripMenuItem"
        Me.PersonalToolStripMenuItem.Size = New System.Drawing.Size(75, 22)
        Me.PersonalToolStripMenuItem.Text = "Personal"
        '
        'UsuarioToolStripMenuItem1
        '
        Me.UsuarioToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarToolStripMenuItem1, Me.ListaDeUsuariosToolStripMenuItem})
        Me.UsuarioToolStripMenuItem1.Image = CType(resources.GetObject("UsuarioToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.UsuarioToolStripMenuItem1.Name = "UsuarioToolStripMenuItem1"
        Me.UsuarioToolStripMenuItem1.Size = New System.Drawing.Size(125, 22)
        Me.UsuarioToolStripMenuItem1.Text = "Usuario"
        '
        'AgregarToolStripMenuItem1
        '
        Me.AgregarToolStripMenuItem1.Image = CType(resources.GetObject("AgregarToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.AgregarToolStripMenuItem1.Name = "AgregarToolStripMenuItem1"
        Me.AgregarToolStripMenuItem1.Size = New System.Drawing.Size(182, 22)
        Me.AgregarToolStripMenuItem1.Text = "Agregar"
        '
        'ListaDeUsuariosToolStripMenuItem
        '
        Me.ListaDeUsuariosToolStripMenuItem.Image = CType(resources.GetObject("ListaDeUsuariosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ListaDeUsuariosToolStripMenuItem.Name = "ListaDeUsuariosToolStripMenuItem"
        Me.ListaDeUsuariosToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ListaDeUsuariosToolStripMenuItem.Text = "Lista de Usuarios"
        '
        'ChoferToolStripMenuItem1
        '
        Me.ChoferToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarToolStripMenuItem3, Me.ListaDeChoferesToolStripMenuItem})
        Me.ChoferToolStripMenuItem1.Image = CType(resources.GetObject("ChoferToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.ChoferToolStripMenuItem1.Name = "ChoferToolStripMenuItem1"
        Me.ChoferToolStripMenuItem1.Size = New System.Drawing.Size(125, 22)
        Me.ChoferToolStripMenuItem1.Text = "Chofer"
        '
        'AgregarToolStripMenuItem3
        '
        Me.AgregarToolStripMenuItem3.Image = CType(resources.GetObject("AgregarToolStripMenuItem3.Image"), System.Drawing.Image)
        Me.AgregarToolStripMenuItem3.Name = "AgregarToolStripMenuItem3"
        Me.AgregarToolStripMenuItem3.Size = New System.Drawing.Size(184, 22)
        Me.AgregarToolStripMenuItem3.Text = "Agregar"
        '
        'ListaDeChoferesToolStripMenuItem
        '
        Me.ListaDeChoferesToolStripMenuItem.Image = CType(resources.GetObject("ListaDeChoferesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ListaDeChoferesToolStripMenuItem.Name = "ListaDeChoferesToolStripMenuItem"
        Me.ListaDeChoferesToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.ListaDeChoferesToolStripMenuItem.Text = "Lista de Choferes"
        '
        'ConfiguracionToolStripMenuItem
        '
        Me.ConfiguracionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ParametroToolStripMenuItem, Me.PuntosToolStripMenuItem, Me.HojaRutaToolStripMenuItem, Me.FlotaToolStripMenuItem})
        Me.ConfiguracionToolStripMenuItem.Name = "ConfiguracionToolStripMenuItem"
        Me.ConfiguracionToolStripMenuItem.Size = New System.Drawing.Size(67, 22)
        Me.ConfiguracionToolStripMenuItem.Text = "Edición"
        '
        'ParametroToolStripMenuItem
        '
        Me.ParametroToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarToolStripMenuItem})
        Me.ParametroToolStripMenuItem.Image = CType(resources.GetObject("ParametroToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ParametroToolStripMenuItem.Name = "ParametroToolStripMenuItem"
        Me.ParametroToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ParametroToolStripMenuItem.Text = "Parametro"
        '
        'AgregarToolStripMenuItem
        '
        Me.AgregarToolStripMenuItem.Image = CType(resources.GetObject("AgregarToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AgregarToolStripMenuItem.Name = "AgregarToolStripMenuItem"
        Me.AgregarToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.AgregarToolStripMenuItem.Text = "Agregar "
        '
        'PuntosToolStripMenuItem
        '
        Me.PuntosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarToolStripMenuItem2, Me.ListaPuntosToolStripMenuItem})
        Me.PuntosToolStripMenuItem.Image = CType(resources.GetObject("PuntosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PuntosToolStripMenuItem.Name = "PuntosToolStripMenuItem"
        Me.PuntosToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PuntosToolStripMenuItem.Text = "Puntos"
        '
        'AgregarToolStripMenuItem2
        '
        Me.AgregarToolStripMenuItem2.Name = "AgregarToolStripMenuItem2"
        Me.AgregarToolStripMenuItem2.Size = New System.Drawing.Size(151, 22)
        Me.AgregarToolStripMenuItem2.Text = "Agregar"
        '
        'ListaPuntosToolStripMenuItem
        '
        Me.ListaPuntosToolStripMenuItem.Name = "ListaPuntosToolStripMenuItem"
        Me.ListaPuntosToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ListaPuntosToolStripMenuItem.Text = "Lista Puntos"
        '
        'HojaRutaToolStripMenuItem
        '
        Me.HojaRutaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarToolStripMenuItem6, Me.ListaRutasToolStripMenuItem})
        Me.HojaRutaToolStripMenuItem.Image = CType(resources.GetObject("HojaRutaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.HojaRutaToolStripMenuItem.Name = "HojaRutaToolStripMenuItem"
        Me.HojaRutaToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.HojaRutaToolStripMenuItem.Text = "Hoja Ruta"
        '
        'AgregarToolStripMenuItem6
        '
        Me.AgregarToolStripMenuItem6.Name = "AgregarToolStripMenuItem6"
        Me.AgregarToolStripMenuItem6.Size = New System.Drawing.Size(152, 22)
        Me.AgregarToolStripMenuItem6.Text = "Agregar"
        '
        'ListaRutasToolStripMenuItem
        '
        Me.ListaRutasToolStripMenuItem.Name = "ListaRutasToolStripMenuItem"
        Me.ListaRutasToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ListaRutasToolStripMenuItem.Text = "Lista Rutas"
        '
        'FlotaToolStripMenuItem
        '
        Me.FlotaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevaFlotaToolStripMenuItem1, Me.ListaFlotasToolStripMenuItem})
        Me.FlotaToolStripMenuItem.Image = CType(resources.GetObject("FlotaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FlotaToolStripMenuItem.Name = "FlotaToolStripMenuItem"
        Me.FlotaToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.FlotaToolStripMenuItem.Text = "Flota"
        '
        'NuevaFlotaToolStripMenuItem1
        '
        Me.NuevaFlotaToolStripMenuItem1.Image = CType(resources.GetObject("NuevaFlotaToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.NuevaFlotaToolStripMenuItem1.Name = "NuevaFlotaToolStripMenuItem1"
        Me.NuevaFlotaToolStripMenuItem1.Size = New System.Drawing.Size(150, 22)
        Me.NuevaFlotaToolStripMenuItem1.Text = "Nueva Flota"
        '
        'ListaFlotasToolStripMenuItem
        '
        Me.ListaFlotasToolStripMenuItem.Image = CType(resources.GetObject("ListaFlotasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ListaFlotasToolStripMenuItem.Name = "ListaFlotasToolStripMenuItem"
        Me.ListaFlotasToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.ListaFlotasToolStripMenuItem.Text = "Lista Flotas"
        '
        'ConfiguracionToolStripMenuItem1
        '
        Me.ConfiguracionToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PermisosToolStripMenuItem, Me.TipoDeUsuarioToolStripMenuItem})
        Me.ConfiguracionToolStripMenuItem1.Name = "ConfiguracionToolStripMenuItem1"
        Me.ConfiguracionToolStripMenuItem1.Size = New System.Drawing.Size(109, 22)
        Me.ConfiguracionToolStripMenuItem1.Text = "Configuración"
        '
        'PermisosToolStripMenuItem
        '
        Me.PermisosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarToolStripMenuItem4})
        Me.PermisosToolStripMenuItem.Image = CType(resources.GetObject("PermisosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PermisosToolStripMenuItem.Name = "PermisosToolStripMenuItem"
        Me.PermisosToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.PermisosToolStripMenuItem.Text = "Permisos"
        '
        'AgregarToolStripMenuItem4
        '
        Me.AgregarToolStripMenuItem4.Image = CType(resources.GetObject("AgregarToolStripMenuItem4.Image"), System.Drawing.Image)
        Me.AgregarToolStripMenuItem4.Name = "AgregarToolStripMenuItem4"
        Me.AgregarToolStripMenuItem4.Size = New System.Drawing.Size(128, 22)
        Me.AgregarToolStripMenuItem4.Text = "Agregar"
        '
        'TipoDeUsuarioToolStripMenuItem
        '
        Me.TipoDeUsuarioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarToolStripMenuItem5})
        Me.TipoDeUsuarioToolStripMenuItem.Image = CType(resources.GetObject("TipoDeUsuarioToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TipoDeUsuarioToolStripMenuItem.Name = "TipoDeUsuarioToolStripMenuItem"
        Me.TipoDeUsuarioToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.TipoDeUsuarioToolStripMenuItem.Text = "Tipo de Usuario"
        '
        'AgregarToolStripMenuItem5
        '
        Me.AgregarToolStripMenuItem5.Name = "AgregarToolStripMenuItem5"
        Me.AgregarToolStripMenuItem5.Size = New System.Drawing.Size(128, 22)
        Me.AgregarToolStripMenuItem5.Text = "Agregar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Courier New", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label3.Location = New System.Drawing.Point(22, 299)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(448, 69)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Encomiendas"
        '
        'Timer1
        '
        '
        'lbl_hora
        '
        Me.lbl_hora.AutoSize = True
        Me.lbl_hora.BackColor = System.Drawing.Color.Transparent
        Me.lbl_hora.Font = New System.Drawing.Font("Courier New", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_hora.ForeColor = System.Drawing.Color.Gainsboro
        Me.lbl_hora.Location = New System.Drawing.Point(909, 26)
        Me.lbl_hora.Name = "lbl_hora"
        Me.lbl_hora.Size = New System.Drawing.Size(164, 39)
        Me.lbl_hora.TabIndex = 6
        Me.lbl_hora.Text = " . . . "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Courier New", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label5.Location = New System.Drawing.Point(104, 368)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(258, 69)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "UDABOL"
        '
        'MDIPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BackgroundImage = Global.Parametro.My.Resources.Resources._as
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1077, 598)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lbl_hora)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "MDIPrincipal"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sistema de Encomienda"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguracionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ParametroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IniciarSesiónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CerrarSesiónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EncomiendaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevaEncomiendaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PersonalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuarioToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChoferToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PuntosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HojaRutaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarToolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguracionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PermisosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tss_fecha As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TipoDeUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AgregarToolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaDeUsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaDeChoferesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents FlotaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevaFlotaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaFlotasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents HistorialEncomiendaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaPuntosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaEncomiendasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaRutasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lbl_hora As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
