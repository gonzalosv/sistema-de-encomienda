﻿Public Class Frm_Permisos

    Public ID_TipoUsuarioAux As Integer = 0

    Private Sub btn_cancelar_Click(sender As Object, e As EventArgs) Handles btn_cancelar.Click
        Me.Close()
    End Sub

    Sub Llenar_cmbTiposUsuarios()
        Try
            Dim _user As New LCN.Usuario
            Dim Tabla As New DataTable

            Tabla = _user.ObtenerTiposDeUsuarios("", True)

            If Not IsNothing(Tabla) Then
                Me.cmbTipoUsuarios.DataSource = Tabla
                Me.cmbTipoUsuarios.DisplayMember = "Descripcion"
                Me.cmbTipoUsuarios.ValueMember = "ID_Tipo"
            Else
                Me.cmbTipoUsuarios.DataSource = Nothing
            End If
        Catch ex As Exception
            MsgBox("Error al llenar el combo de tipos de usuarios", MsgBoxStyle.Critical, "ENCOMIENDAS")
        End Try
    End Sub

    Sub Llenar_dgPermisos(ByVal ID_TipoUser As Integer)
        Try
            Dim _user As New LCN.Usuario
            Dim Tabla As New DataTable

            Tabla = _user.ObtenerPermisos(ID_TipoUser)

            If Not IsNothing(Tabla) Then
                Me.dgPermisos.DataSource = Tabla
                Me.dgPermisos.ClearSelection()
            Else
                For i As Integer = 0 To Me.dgPermisos.RowCount - 1
                    Me.dgPermisos.Rows.RemoveAt(0)
                Next
            End If

        Catch ex As Exception
            MsgBox("Error al intentar llenar los permisos", MsgBoxStyle.Critical, "ENCOMIENDAS")
        End Try
    End Sub
    Private Sub Frm_Permisos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Llenar_cmbTiposUsuarios()
        If ID_TipoUsuarioAux = 0 Then
            Me.Llenar_dgPermisos(1)
        Else
            Me.cmbTipoUsuarios.SelectedValue = ID_TipoUsuarioAux
            Me.Llenar_dgPermisos(ID_TipoUsuarioAux)

        End If

    End Sub

    Private Sub cmbTipoUsuarios_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmbTipoUsuarios.SelectionChangeCommitted
        Me.Llenar_dgPermisos(Me.cmbTipoUsuarios.SelectedValue)
    End Sub

    Private Sub btn_guardar_Click(sender As Object, e As EventArgs) Handles btn_guardar.Click
        Try
            Dim ListaComandos As New List(Of SqlClient.SqlCommand)
            Dim Ejec As New LCD.CAD
            Dim _user As New LCN.Usuario

            For Each Fila As DataGridViewRow In Me.dgPermisos.Rows
                ListaComandos.Add(_user.ActualizaPermiso(Me.cmbTipoUsuarios.SelectedValue, Fila.Cells("ID_Ventana").Value, Fila.Cells("Acceso").Value))
            Next

            If Ejec.EjecutarTransaccion(ListaComandos) = True Then
                MsgBox("Permisos modificados correctamente.", MsgBoxStyle.Information, "ENCOMIENDA")
                MDIPrincipal.Establecer_Permisos()
            Else
                MsgBox("Los Cambios solicitado no fueron realizados.Consulte con el Programador.", MsgBoxStyle.Critical, "ENCOMIENDA")
            End If

        Catch ex As Exception
            MsgBox(ex.Message + " Error al intentar establecer los permisos", MsgBoxStyle.Critical, "ENCOMIENDAS")
        End Try
    End Sub

    Private Sub cmbTipoUsuarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTipoUsuarios.SelectedIndexChanged

    End Sub
End Class