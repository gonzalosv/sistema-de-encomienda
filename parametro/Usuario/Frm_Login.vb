﻿Public Class Frm_Login
    Private Sub PierdeFoco(ByVal sender As TextBox, ByVal e As System.EventArgs) Handles txt_username.LostFocus, txt_password.LostFocus
        sender.BackColor = Color.White
        sender.SelectAll()
    End Sub

    Private Sub AgarraFoco(ByVal sender As TextBox, ByVal e As System.EventArgs) Handles txt_username.GotFocus, txt_password.GotFocus
        sender.BackColor = Color.WhiteSmoke
        sender.SelectAll()
    End Sub
    Private Sub GroupBox1_Enter(sender As System.Object, e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub btn_cancelar_Click(sender As Object, e As EventArgs) Handles btn_cancelar.Click
        Me.Close()
    End Sub

    Private Sub btn_guardar_Click(sender As Object, e As EventArgs)
        Try
            Dim _user As New LCN.Usuario
            Dim Tabla As New DataTable

            Tabla = _user.VerificarExiste(Me.txt_username.Text, Me.txt_password.Text)
            If Not IsNothing(Tabla) Then
                sesion_idUsuario = Tabla.Rows(0).Item("ID_Usuario")
                sesion_TipoUser = Tabla.Rows(0).Item("ID_TipoUsuario")
                ID_PuntoUsuario = Tabla.Rows(0).Item("ID_Punto")
                MDIPrincipal.Establecer_Permisos()
                MsgBox("BIENVENIDO : " & Me.txt_username.Text)
                Me.Close()
            Else
                MsgBox("Datos incorrectos, intente nuevamente.", MsgBoxStyle.Critical)
            End If

        Catch ex As Exception
            MsgBox("Error al intentar iniciar sesion", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Frm_Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btn_ingresar_Click(sender As Object, e As EventArgs) Handles btn_ingresar.Click
        Try
            Dim _user As New LCN.Usuario
            Dim Tabla As New DataTable

            Tabla = _user.VerificarExiste(Me.txt_username.Text, Me.txt_password.Text)
            If Not IsNothing(Tabla) Then
                MsgBox("BIENVENIDO NUEVAMENTE " & UCase(Me.txt_username.Text), MsgBoxStyle.Information, "SISTEMA ENCOMIENDAS")
                sesion_idUsuario = Tabla.Rows(0).Item("ID_Usuario")
                sesion_TipoUser = Tabla.Rows(0).Item("ID_TipoUsuario")
                '    ID_PuntoUsuario = Tabla.Rows(0).Item("ID_Punto")
                MDIPrincipal.Establecer_Permisos()
                Me.Close()
            Else
                MsgBox("Datos incorrectos, intente nuevamente.", MsgBoxStyle.Critical)
            End If

        Catch ex As Exception
            MsgBox("Error al intentar iniciar sesion", MsgBoxStyle.Critical)
        End Try
    End Sub
End Class