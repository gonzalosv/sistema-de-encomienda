﻿Public Class Frm_ListaUsuarios

    Private Sub btn_cancelar_Click(sender As Object, e As EventArgs) Handles btn_cancelar.Click
        Me.Close()
    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub Frm_ListaUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.cmb_estado.SelectedIndex = 1

        Me.LLenar_dgUsuarios(Me.cmb_estado.SelectedIndex, Me.txt_buscar.Text)
    End Sub

    Sub LLenar_dgUsuarios(ByVal Estado As Boolean,
                          ByVal Nombre As String)

        Try
            Dim _user As New LCN.Usuario
            Dim Tabla As New DataTable

            Tabla = _user.ObtenerTodosByNombre(Estado, Nombre)

            If Not IsNothing(Tabla) Then
                Me.dgv_lista.DataSource = Tabla
                Me.dgv_lista.ClearSelection()
            Else
                Dim i As Integer
                For i = 0 To Me.dgv_lista.RowCount - 1
                    Me.dgv_lista.Rows.RemoveAt(0)
                Next
            End If
        Catch ex As Exception
            MsgBox("Error al llenar la lista de usuarios")
        End Try

    End Sub

    Private Sub txt_buscar_TextChanged(sender As Object, e As EventArgs) Handles txt_buscar.TextChanged
        Me.LLenar_dgUsuarios(Me.cmb_estado.SelectedIndex, Me.txt_buscar.Text)
    End Sub

    Private Sub cmb_estado_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cmb_estado.SelectionChangeCommitted
        Me.LLenar_dgUsuarios(Me.cmb_estado.SelectedIndex, Me.txt_buscar.Text)
    End Sub

    Private Sub btn_nuevo_Click(sender As Object, e As EventArgs) Handles btn_nuevo.Click
        Dim frm As New Frm_Usuario
        frm.ShowDialog()
        Me.LLenar_dgUsuarios(Me.cmb_estado.SelectedIndex, Me.txt_buscar.Text)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim usu As New LCN.Usuario
        Dim con As New LCD.CAD
        Me.Cursor = Cursors.WaitCursor
        If usu.migraUsuarios(con.db_host) Then
            MsgBox("Usuarios migrados de forma correcta!", MsgBoxStyle.Information, "ENCOMIENDA")
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btn_reporte_Click(sender As Object, e As EventArgs) Handles btn_reporte.Click
        Try
            If Me.dgv_lista.RowCount - 1 < 0 Then
                MsgBox("No tiene ningún reporte disponible.", MsgBoxStyle.Critical, "ENCOMIENDAS")
            Else

                Dim frmReport As New frmVisualizadorRPT
                Me.Cursor = Cursors.WaitCursor

                Dim _usu As New LCN.Usuario
                Dim DT As New DataTable

                DT = _usu.ObtenerTodosByNombre(Me.cmb_estado.SelectedIndex, Me.txt_buscar.Text)

                If IsNothing(DT) Then
                    Me.Cursor = Cursors.Default
                    MsgBox("No Existen Datos a Mostrar", MsgBoxStyle.Information, "ENCOMIENDAS")
                Else
                    Dim RepTemp As New rptUsuarios
                    RepTemp.SetDataSource(DT)
                    frmReport.crviewer.ReportSource = RepTemp
                    frmReport.ShowDialog()
                    RepTemp.Dispose()

                End If
                Me.Cursor = Cursors.Default

            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("ERROR DESDE :: " & ex.Source & vbNewLine & " MENSAJE :: " & ex.Message, MsgBoxStyle.Critical, "ENCOMIENDAS")
        End Try
    End Sub
End Class