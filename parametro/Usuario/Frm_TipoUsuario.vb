﻿Public Class Frm_TipoUsuario


    Private Sub btn_cancelar_Click(sender As Object, e As EventArgs) Handles btn_cancelar.Click
        Me.Close()
    End Sub

    Private Sub btn_guardar_Click(sender As Object, e As EventArgs) Handles btn_guardar.Click
        If Me.txtDescripcion.Text <> "" Then
            Dim _tipo As New LCN.Usuario

            If _tipo.GuardaTipoUser(Me.txtDescripcion.Text, True, Now) = True Then
                MsgBox("Tipo de usuario creado de forma correcta!", MsgBoxStyle.Information, "SISTEMA ENCOMIENDAS")
                Me.Llenar_dgTipoUser(Me.txt_buscar.Text, Me.cmb_estado.SelectedIndex)
            Else
                MsgBox("Error al intentar guardar el nuevo tipo de usuario", MsgBoxStyle.Critical, "SISTEMA ENCOMIENDAS")
            End If
        Else
            MsgBox("Debe especificar la descripcion del tipo de usuario", MsgBoxStyle.Critical, "SISTEMA ENCOMIENDAS")
            Me.txtDescripcion.Focus()
        End If
    End Sub

    Sub Llenar_dgTipoUser(ByVal Descripcion As String,
                          ByVal Estado As Boolean)
        Dim _tipo As New LCN.Usuario
        Dim tabla As New DataTable

        tabla = _tipo.ObtenerTiposDeUsuarios(Descripcion, Estado)

        If Not IsNothing(tabla) Then
            Me.dg_TipoUser.DataSource = tabla
            Me.dg_TipoUser.ClearSelection()
        Else
            For i As Integer = 0 To Me.dg_TipoUser.RowCount - 1
                Me.dg_TipoUser.Rows.RemoveAt(0)
            Next
        End If
    End Sub

    Private Sub Frm_TipoUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.cmb_estado.SelectedIndex = 1
        Me.Llenar_dgTipoUser(Me.txt_buscar.Text, True)
    End Sub

    Private Sub cmb_estado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmb_estado.SelectedIndexChanged
        Me.Llenar_dgTipoUser(Me.txt_buscar.Text, Me.cmb_estado.SelectedIndex)
    End Sub

    Private Sub txt_buscar_TextChanged(sender As Object, e As EventArgs) Handles txt_buscar.TextChanged
        Me.Llenar_dgTipoUser(Me.txt_buscar.Text, Me.cmb_estado.SelectedIndex)
    End Sub

    Private Sub dg_TipoUser_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dg_TipoUser.CellClick
        If Me.dg_TipoUser.Columns(e.ColumnIndex).Name = "Permisos" Then
            Dim frm As New Frm_Permisos
            frm.ID_TipoUsuarioAux = Me.dg_TipoUser.CurrentRow.Cells("ID_Tipo").Value
            Me.Opacity = 0
            frm.ShowDialog()
            Me.Opacity = 100
            Me.Llenar_dgTipoUser(Me.txt_buscar.Text, Me.cmb_estado.SelectedIndex)
            MDIPrincipal.Establecer_Permisos()
        End If
    End Sub

End Class