﻿Public Class Frm_Usuario

    Private Sub btn_cancelar_Click(sender As Object, e As EventArgs) Handles btn_cancelar.Click
        Me.Close()
    End Sub

    'sub llenar_cmbpuntos()
    '    try
    '        dim _pun as new lcn.puntos
    '        dim tabla as new datatable

    '        tabla = _pun.obtener(true, "")
    '        me.cmb_punto.datasource = nothing
    '        me.cmb_punto.datasource = tabla
    '        me.cmb_punto.valuemember = "id_punto"
    '        me.cmb_punto.displaymember = "descripcion"

    '    catch ex as exception
    '        msgbox("error al llenar el combo de puntos")
    '    end try
    'end sub

    Sub Llenar_cmbTipo()
        Try
            Dim _user As New LCN.Usuario
            Dim Tabla As New DataTable

            Tabla = _user.ObtenerTiposDeUsuarios("", True)


            If Not IsNothing(Tabla) Then
                Me.cmb_tipousuario.DataSource = Tabla
                Me.cmb_tipousuario.DisplayMember = "Descripcion"
                Me.cmb_tipousuario.ValueMember = "ID_Tipo"
            Else
                Me.cmb_tipousuario.DataSource = Nothing
            End If


        Catch ex As Exception
            MsgBox("Error al llenar el combo de tipos de usuarios")
        End Try
    End Sub

    Private Sub Frm_Usuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Me.Llenar_cmbPuntos()
        Me.Llenar_cmbTipo()
    End Sub

    Private Sub btn_guardar_Click(sender As Object, e As EventArgs) Handles btn_guardar.Click
        Try
            If Me.txt_usuario.Text <> "" Then
                If Me.txt_contrasena.Text <> "" Then
                    Dim _user As New LCN.Usuario
                    Dim tabla As New DataTable

                    tabla = _user.VerificarExiste(Me.txt_usuario.Text, Me.txt_contrasena.Text)
                    If Not IsNothing(tabla) Then
                        MsgBox("Error al crear el usuario, por favor intente con otros datos.", MsgBoxStyle.Critical, "ENCOMIENDAS")
                    Else
                        If _user.Guardar(Me.txt_usuario.Text, Me.txt_contrasena.Text, Me.cmb_tipousuario.SelectedValue) Then
                            MsgBox("Usuario creado de forma correcta!!!", MsgBoxStyle.Information, "ENCOMIENDAS")
                            Me.Close()
                        Else
                            MsgBox("Error as intentar guardar los datos del nuevo usuario", MsgBoxStyle.Critical, "ENCOMIENDAS")
                        End If
                    End If
                Else
                    MsgBox("Debe ingresar la contraseña del usuario", MsgBoxStyle.Critical, "ENCOMIENDAS")
                    Me.txt_contrasena.Focus()
                End If
            Else
                MsgBox("Debe Ingresar el nombre del usuario", MsgBoxStyle.Critical, "ENCOMIENDAS")
                Me.txt_usuario.Focus()
            End If
        Catch ex As Exception
            MsgBox("Error al intentar guardar un usuario")
        End Try
    End Sub


    Private Sub cbMostrar_CheckedChanged(sender As Object, e As EventArgs) Handles cbMostrar.CheckedChanged
        If Me.cbMostrar.Checked = False Then
            Me.txt_contrasena.PasswordChar = "?"
        Else
            Me.txt_contrasena.PasswordChar = ""
        End If
    End Sub
End Class