﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data

Public Class frmVisualizadorRPT

    Dim Existen_Datos As Boolean = False

    Public Property ExistenDatos() As Boolean
        Get
            Return Existen_Datos
        End Get
        Set(ByVal value As Boolean)
            Existen_Datos = value
        End Set
    End Property

    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub


    Private Sub frmVisualizadorRPT_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.crviewer.Zoom(100)
    End Sub
End Class