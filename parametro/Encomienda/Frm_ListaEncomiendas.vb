﻿Public Class Frm_ListaEncomiendas

    Dim hr As New LCN.HojaRuta
    Sub CargarOrigen()
        Dim TablaPuntos As New DataTable
        TablaPuntos = hr.BusquedaOrigen()
        If Not IsNothing(TablaPuntos) Then
            Me.cbOrigen.DataSource = TablaPuntos
            Me.cbOrigen.DisplayMember = "Descripcion"
            Me.cbOrigen.ValueMember = "ID_Punto"
        Else
        End If
    End Sub

    Sub CargarDestino(ByVal des As Integer)
        Dim TablaPuntos As New DataTable
        TablaPuntos = hr.BusquedaDestino(des)
        If Not IsNothing(TablaPuntos) Then
            Me.cbDestino.DataSource = TablaPuntos
            Me.cbDestino.DisplayMember = "Descripcion"
            Me.cbDestino.ValueMember = "ID_Punto"
        Else
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim pun As New LCN.Puntos
        Dim con As New LCD.CAD
        Me.Cursor = Cursors.WaitCursor
        If pun.migrarencomiendas(con.db_host) Then
            MsgBox("Encomiendas migradas de forma correcta!", MsgBoxStyle.Information, "ENCOMIENDA")
        End If
        Me.Llenar_dgEncomiendas(Me.cbOrigen.SelectedValue, cbDestino.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub frmListaEncomiendas_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            Dim cnx As New LCD.CAD

            If cnx.ID_PuntoGlobal = 1 Then
                Me.cbOrigen.Enabled = True
            Else
                Me.cbOrigen.Enabled = False
            End If

            CargarOrigen()

            Me.cbOrigen.SelectedValue = cnx.ID_PuntoGlobal
            CargarDestino(cnx.ID_PuntoGlobal)

            If sesion_TipoUser <> 1 Then
                Me.dtpFechaIni.Enabled = False
                Me.dtpFechaFin.Enabled = False
                Me.chb_destino.Enabled = False
                Me.cbOrigen.Enabled = False
            End If
            Me.Llenar_dgEncomiendas(Me.cbOrigen.SelectedValue, cbDestino.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)
            Me.dtpFechaIni.Value = Now
            Me.dtpFechaFin.Value = Now
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Sub Llenar_dgEncomiendas(ByVal origen As String,
                                      ByVal destino As String,
                                      ByVal f_ini As Date,
                                      ByVal f_fin As Date)
        Try
            Dim Encom As New LCN.Encomienda
            Dim Tabla As New DataTable

            f_ini = dtpFechaIni.Value.Date + " 00:00:00"
            f_fin = dtpFechaFin.Value.Date + " 23:59:59"

            If (cbDestino.Enabled = False) Then
                Tabla = Encom.ObtenerEncomienda(origen, "", f_ini, f_fin)
            Else
                Tabla = Encom.ObtenerEncomienda(origen, destino, f_ini, f_fin)
            End If

            If Not IsNothing(Tabla) Then
                Me.dg_Encomiendas.DataSource = Tabla
                Me.dg_Encomiendas.ClearSelection()
            Else
                For i As Integer = 0 To Me.dg_Encomiendas.RowCount - 1
                    Me.dg_Encomiendas.Rows.RemoveAt(0)
                Next

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btn_cancelar_Click(sender As Object, e As EventArgs) Handles btn_cancelar.Click
        'cierra
        Me.Close()

    End Sub

    Private Sub btn_reporte_Click(sender As Object, e As EventArgs) Handles btn_reporte.Click
        Try

            Dim frmReport As New frmVisualizadorRPT
            Me.Cursor = Cursors.WaitCursor

            Dim _enc As New LCN.Encomienda
            Dim DT As New DataTable

            If (cbDestino.Enabled = False) Then
                DT = _enc.ObtenerEncomienda(Me.cbOrigen.SelectedValue, "", dtpFechaIni.Value.Date + " 00:00:00", dtpFechaFin.Value.Date + " 23:59:59")
            Else
                DT = _enc.ObtenerEncomienda(Me.cbOrigen.SelectedValue, Me.cbDestino.SelectedValue, dtpFechaIni.Value.Date + " 00:00:00", dtpFechaFin.Value.Date + " 23:59:59")
            End If

            If IsNothing(DT) Then
                Me.Cursor = Cursors.Default
                MsgBox("No Existen Datos a Mostrar", MsgBoxStyle.Information, "ENCOMIENDAS")
            Else
                Dim RepTemp As New rptEncomHojaRuta1
                RepTemp.SetDataSource(DT)
                frmReport.crviewer.ReportSource = RepTemp
                frmReport.ShowDialog()
                RepTemp.Dispose()

            End If
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("ERROR DESDE :: " & ex.Source & vbNewLine & " MENSAJE :: " & ex.Message, MsgBoxStyle.Critical, "ENCOMIENDAS")
        End Try
    End Sub


    Private Sub cbOrigen_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cbOrigen.SelectionChangeCommitted
        CargarDestino(cbOrigen.SelectedValue)
        Me.Llenar_dgEncomiendas(Me.cbOrigen.SelectedValue, cbDestino.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)
    End Sub

    Private Sub chb_destino_CheckedChanged(sender As Object, e As EventArgs) Handles chb_destino.CheckedChanged
        If (Me.chb_destino.Checked = True) Then
            cbDestino.Enabled = True
            Me.Llenar_dgEncomiendas(Me.cbOrigen.SelectedValue, cbDestino.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)

        Else
            cbDestino.Enabled = False
            Me.Llenar_dgEncomiendas(Me.cbOrigen.SelectedValue, cbDestino.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)

        End If
    End Sub

    Private Sub dtpFechaIni_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaIni.ValueChanged
        Me.Llenar_dgEncomiendas(Me.cbOrigen.SelectedValue, cbDestino.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)
    End Sub

    Private Sub dtpFechaFin_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaFin.ValueChanged
        Me.Llenar_dgEncomiendas(Me.cbOrigen.SelectedValue, cbDestino.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)
    End Sub

    Private Sub cbDestino_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cbDestino.SelectionChangeCommitted
        Me.Llenar_dgEncomiendas(Me.cbOrigen.SelectedValue, cbDestino.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)
    End Sub
End Class