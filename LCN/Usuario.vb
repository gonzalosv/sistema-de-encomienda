﻿
Imports System.Data.SqlClient

Public Class Usuario
    Inherits LCD.CAD


#Region "METODOS"


    ''' <summary>
    ''' Guarda un nuevo Usuario
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Guardar(ByVal Usuario As String,
                            ByVal Contraseña As String,
                            ByVal ID_TipoUsuario As Integer) As Boolean
        Try
            IniciarSP("User_Set")
            AddParametro("@Usuario", Usuario)
            AddParametro("@Contraseña", Contraseña)
            AddParametro("@ID_TipoUsuario", ID_TipoUsuario)

            If EjecutarTransaccion() Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error en : " & ex.Message & vbNewLine & "Desde : " & ex.Source & ex.ToString, MsgBoxStyle.Critical, "DANIEL ROMERO BACOTICH")
            Return False
        End Try

    End Function


    ''' <summary>
    ''' Obtiene todos los usuarios por su nombre
    ''' </summary>
    ''' <param name="Estado">Estado del usuario Activo o Inactivo</param>
    ''' <param name="Nombre">Nombre de los usuarios a buscar, 'en blanco busca todos'</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerTodosByNombre(ByVal Estado As Boolean,
                                         ByVal Nombre As String) As DataTable

        Dim Tabla As New DataTable

        IniciarSP("User_GetAll")
        AddParametro("@Estado", Estado)
        AddParametro("@Nombre", Nombre)

        If EjecutarTransaccion() = True Then
            If getTabla(Tabla) = True Then
                Return Tabla
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' Verifica si existe un usuario por su nombre y contraseña
    ''' </summary>
    ''' <param name="Usuario">Nombre</param>
    ''' <param name="Contraseña">Contraseña</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function VerificarExiste(ByVal Usuario As String, ByVal Contraseña As String) As DataTable

        Dim Tabla As New DataTable

        IniciarSP("User_Verificar")
        AddParametro("@Usuario", Usuario)
        AddParametro("@Contraseña", Contraseña)

        If EjecutarTransaccion() = True Then
            If getTabla(Tabla) = True Then
                If Tabla.Rows.Count > 0 Then
                    Return Tabla
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' Guarda los datos de un tipo de usuario
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GuardaTipoUser(ByVal Descripcion As String,
                                   ByVal Activo As Boolean,
                                   ByVal FechaAct As Date) As Boolean
        Try
            IniciarSP("TipoUser_Set")
            AddParametro("@Descripcion", Descripcion)
            AddParametro("@Activo", Activo)
            AddParametro("@FechaAct", FechaAct)

            If EjecutarTransaccion() Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error en : " & ex.Message & vbNewLine & "Desde : " & ex.Source & ex.ToString, MsgBoxStyle.Critical, "DANIEL ROMERO BACOTICH")
            Return False
        End Try

    End Function

    ''' <summary>
    ''' Obtiene todos los tipos de usuarios
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerTiposDeUsuarios(ByVal Descripcion As String,
                                           ByVal Estado As Boolean) As DataTable

        Dim Tabla As New DataTable

        IniciarSP("TipoUser_Get")
        AddParametro("@Descripcion", Descripcion)
        AddParametro("@Estado", Estado)

        If EjecutarTransaccion() = True Then
            If getTabla(Tabla) = True Then
                Return Tabla
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' Obtiene los permisos de un tipo de usuario
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ObtenerPermisos(ByVal ID_TipoUser As Integer) As DataTable

        Dim Tabla As New DataTable

        IniciarSP("TipoUser_PermisosGet")
        AddParametro("@ID_TipoUser", ID_TipoUser)


        If EjecutarTransaccion() = True Then
            If getTabla(Tabla) = True Then
                Return Tabla
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function

    ''' <summary>
    ''' Actualiza el acceso de un tipo de usuario a una ventana
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ActualizaPermiso(ByVal ID_TipoUser As Integer,
                                     ByVal ID_Ventana As Integer,
                                     ByVal Acceso As Boolean) As SqlCommand

        Dim comando As New SqlCommand
        Dim Tabla As New DataTable

        comando = IniciarSP1("TipoUser_PermisosUpdate")
        AddParametro1(comando, "@ID_TipoUser", ID_TipoUser)
        AddParametro1(comando, "@ID_Ventana", ID_Ventana)
        AddParametro1(comando, "@Acceso", Acceso)

        Return comando

    End Function

    Public Function migraUsuarios(ByVal InstanciaRecibida As String) As Boolean
        Try
            IniciarSP("migracionUsuarios")
            AddParametro("@instanciaenviada", InstanciaRecibida)

            If EjecutarTransaccion() Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error en : " & ex.Message & vbNewLine & "Desde : " & ex.Source & ex.ToString, MsgBoxStyle.Critical, "DANIEL ROMERO BACOTICH")
            Return False
        End Try

    End Function
#End Region
End Class
