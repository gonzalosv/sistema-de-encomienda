﻿Imports System.Data.SqlClient

Public Class HistorialEncomienda
    Inherits LCD.CAD

#Region "ATRIBUTOS"
    Private _idEncomienda As String
    Private _idUsuario As Integer
    Private _estado As Integer
    Private _fecha As Date
    Private _confirmado As Boolean
    Private _migrado As Boolean
#End Region


#Region "PROPIEDADES"


    Public Property IdEncomienda() As String
        Get
            Return _idEncomienda
        End Get
        Set(ByVal value As String)
            _idEncomienda = value
        End Set
    End Property


    Public Property IdUsuario() As String
        Get
            Return _idUsuario
        End Get
        Set(ByVal value As String)
            _idUsuario = value
        End Set
    End Property

    Public Property Estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property


    Public Property Fecha() As String
        Get
            Return _fecha
        End Get
        Set(ByVal value As String)
            _fecha = value
        End Set
    End Property

    Public Property Confirmado() As String
        Get
            Return _confirmado
        End Get
        Set(ByVal value As String)
            _confirmado = value
        End Set
    End Property

    Public Property Migrado() As String
        Get
            Return _migrado
        End Get
        Set(ByVal value As String)
            _migrado = value
        End Set
    End Property

#End Region

#Region "METODOS"

    ''' <summary>
    ''' Crear un nuevo registro el la clase parametro
    ''' </summary>
    ''' <returns>retorna un boolean true si el la registro se ejecuto exitosamente</returns>
    ''' <remarks></remarks>
    Public Function CrearHistorial() As Boolean

        Dim flag As Boolean = False

        Try
            IniciarSP("HistorialEncomienda_SetForUser")
            AddParametro("@_idEncomienda", IdEncomienda)
            AddParametro("@_estado", Estado)
            AddParametro("@_confirmado", Confirmado)
            AddParametro("@_migrado", Migrado)

            If EjecutarTransaccion() Then
                flag = True
            End If

        Catch ex As Exception
            MsgBox("Error al insertar Parametro -->" & ex.Message)
        End Try

        Return flag

    End Function



    Public Function EncomiendasPorHojaRuta(ByVal ID_HojaRuta As Integer, ByVal estado As Char) As DataTable

        Dim Tabla As New DataTable

        IniciarSP("HistorialEncomienda_ListEncomiendaForRuta")
        AddParametro("@_idHojaRuta", ID_HojaRuta)
        AddParametro("@_estado", estado)

        If EjecutarTransaccion() = True Then
            If getTabla(Tabla) = True Then
                Return Tabla
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If

    End Function


#End Region




End Class
